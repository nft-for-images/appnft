import 'package:flutter/material.dart';

const brandColor = Color.fromARGB(255, 0, 59, 32);
const themeColor = Color.fromARGB(255, 7, 107, 189);
const secondaryColor = Color(0xff3c3d46);
const plainColor = Color(0xffffffff);
const dShadeColor = Color(0xff545454);
const dangerColor = Color(0xff800200);
const darkColor = Color(0xff545454);
