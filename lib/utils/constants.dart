import 'package:flutter/material.dart';

const tagLine = "NFT for Images";
const description = "Have a nice day!";
const landingAction = "Next";
const title = "NFT for Images";
List<Map<String, dynamic>> bottomMenu = [
  {'icon': Icons.home, 'label': 'Home'},
  // {'icon': Icons.category, 'label': 'Categories'},
  {'icon': Icons.add, 'label': 'Create NFT'},
  {'icon': Icons.person, 'label': 'My Profile'},
];
const magTitle = "NFT for Images";
const uploadDescription = "Generate Images Cover";
